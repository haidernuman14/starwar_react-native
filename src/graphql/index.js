import { ApolloClient, InMemoryCache } from '@apollo/client';
import { RestLink } from 'apollo-link-rest';

const client = new ApolloClient({
  uri: 'https://graphql.org/swapi-graphql',
  cache: new InMemoryCache(),
  headers:{
    "Content-Type": "application/json"
  },
  credentials: "omit",
});
export  default client;