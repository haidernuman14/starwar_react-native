import { gql } from 'apollo-boost';
export const allPlanets = gql`
query AllPlanets($first:Int){
      allPlanets(first:$first){
    edges{
      node{
        name
        id
        filmConnection{
          edges{
            node{
              title
             episodeID
              director
              
            }
          }
        }
      }
    }
  }
}`;