import React,{useEffect,useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useMutation, useQuery } from '@apollo/react-hooks';
import {allPlanets} from '../graphql/allPlanets';
import data from '../util/data.json'
import RNShake from 'react-native-shake';



export default function Welcome() {

  const [plats,setplats] = useState([]);
  const [filterdata,setFilterData]= useState(null);

    useQuery(allPlanets, {
      variables:{first:30},
      fetchPolicy: "network-only",

        onError: (error) => {console.log("error",error)},
        onCompleted: async (data) => {
            console.log("data",data)
        }  
      })
      useEffect(()=>{
        const subscription = RNShake.addListener(onUndo);
      
        return () => {
          subscription.remove();
        };
      },[])

      const onUndo = ()=>{
          if(data.length>0){
          let myitem =  data[Math.floor(Math.random()*data.length)];
          console.log(myitem)
             setFilterData(myitem)
          }
      }

 

  return (
    <View style={styles.container}>
      
         {filterdata? 
           <View style={{alignItems:"center"}}>
          <Text style={{fontSize:30,fontWeight:"bold"}}>
            {filterdata && filterdata.node.name?filterdata.node.name:""}
          </Text>
            {filterdata.node.filmConnection.edges.length>0 && filterdata.node.filmConnection.edges.map((item,index)=>
            <View style={{alignItems:"center",}}>
            <Text  >
            {item && item.node.title?item.node.title:"film Title not found"}
             </Text>
            </View>
            )}
          </View>:
            <Text style={{fontSize:20,fontWeight:"bold"}}>
              Shake your device
          </Text>
          }
           
         
       
    
     
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop:100
   
  },
});
